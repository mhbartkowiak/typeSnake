var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path='board.ts'/>
var Bone = (function (_super) {
    __extends(Bone, _super);
    function Bone(square) {
        _super.call(this, square.x, square.y, square.div);
        this.is_snake = true;
        square.bone();
        this.unbone = function () { square.unbone(); };
    }
    Bone.prototype.newCoords = function (direction) {
        var dest = Bone.MOVES[direction];
        return {
            x: this.x + dest['x'],
            y: this.y + dest['y']
        };
    };
    Bone.prototype.head = function () {
        this.div.classList.add("head");
    };
    Bone.prototype.unhead = function () {
        this.div.classList.remove("head");
    };
    Bone.MOVES = {
        up: { x: 0, y: -1 },
        down: { x: 0, y: 1 },
        left: { x: -1, y: 0 },
        right: { x: 1, y: 0 }
    };
    return Bone;
}(Square));
//# sourceMappingURL=bone.js.map