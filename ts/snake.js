/// <reference path='square.ts'/>
/// <reference path='bone.ts'/>
var Snake = (function () {
    function Snake(direction) {
        if (direction === void 0) { direction = 'right'; }
        this.bones = [];
        this.lost = false;
        this._direction = direction;
        this._last_direction = direction;
    }
    Snake.prototype.addBone = function (bone) {
        if (this.head()) {
            this.head().unhead();
        }
        bone.head();
        this.bones.unshift(bone);
    };
    Snake.prototype.head = function () {
        return this.bones[0];
    };
    Snake.prototype.move = function () {
        var new_square = Board.findByCoords(this.head().newCoords(this.direction));
        if (new_square.food) {
            new_square.unfood();
            Board.addFood();
        }
        else {
            this.popLast();
        }
        if (new_square.is_snake) {
            alert("Długość węża: " + this.bones.length);
            return this.lose();
        }
        this._last_direction = this._direction;
        this.addBone(new Bone(new_square));
    };
    Object.defineProperty(Snake.prototype, "direction", {
        get: function () {
            return this._direction;
        },
        set: function (dir) {
            if (this.validDirection(dir)) {
                this._direction = dir;
            }
        },
        enumerable: true,
        configurable: true
    });
    Snake.prototype.validDirection = function (dir) {
        if (this._last_direction === dir) {
            return false;
        }
        if (dir === Board.OPPOSITE_DIRECTION[this._last_direction]) {
            return false;
        }
        return true;
    };
    Snake.prototype.popLast = function () {
        this.bones.pop().unbone();
    };
    Snake.prototype.lose = function () {
        this.lost = true;
    };
    Snake.prototype.won = function () {
        return this.bones.length === Board.NUMBER_OF_SQUARES;
    };
    Snake.prototype.serialize = function () {
        return {
            bones: this.bones.map(function (bone) { return bone.coords(); }),
            lost: this.lost,
            direction: this.direction
        };
    };
    Snake.prototype.isHead = function (bone) {
        return this.head() === bone;
    };
    Snake.loadFromMap = function (map) {
        var snake = new Snake(map["direction"]);
        snake.lost = map["lost"];
        map["bones"].reverse().forEach(function (coords) {
            snake.addBone(new Bone(Board.findByCoords(coords)));
        });
        return snake;
    };
    return Snake;
}());
//# sourceMappingURL=snake.js.map