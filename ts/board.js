/// <reference path='square.ts'/>
/// <reference path='snake.ts'/>
var Board = (function () {
    function Board() {
    }
    Board.prepareMatrix = function () {
        Board.matrix = [];
        for (var y = 1; y <= Board.LENGTH; y += 1) {
            Board.matrix[y] = [];
            for (var x = 1; x <= Board.WIDTH; x += 1) {
                Board.matrix[y][x] = new Square(x, y, document.querySelector(".square-" + x + "-" + y));
            }
        }
    };
    Board.find = function (x, y) {
        x = Board.transform(x, "WIDTH");
        y = Board.transform(y, "LENGTH");
        return Board.matrix[y][x];
    };
    Board.findByCoords = function (coords) {
        return Board.find(coords["x"], coords["y"]);
    };
    Board.transform = function (x, dim) {
        if (x === 0) {
            return Board[dim];
        }
        if (x > Board[dim]) {
            return x - Board[dim];
        }
        return x;
    };
    Board.randomSquare = function () {
        var squares = [];
        for (var y = 1; y <= Board.LENGTH; y += 1) {
            for (var x = 1; x <= Board.WIDTH; x += 1) {
                if (Board.matrix[y] && Board.matrix[y][x]) {
                    var square = Board.matrix[y][x];
                    if (!square.is_snake) {
                        squares.push(square);
                    }
                }
            }
        }
        return squares[Math.floor(Math.random() * squares.length)];
    };
    Board.addFood = function () {
        var square = Board.randomSquare();
        if (square) {
            square.makeFood();
        }
    };
    Board.togglePause = function () {
        return Board.paused = !Board.paused;
    };
    Board.OPPOSITE_DIRECTION = {
        left: 'right',
        right: 'left',
        up: 'down',
        down: 'up'
    };
    Board.WIDTH = 50;
    Board.LENGTH = 50;
    Board.NUMBER_OF_SQUARES = Board.WIDTH * Board.LENGTH;
    Board.paused = false;
    return Board;
}());
//# sourceMappingURL=board.js.map