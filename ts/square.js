var Square = (function () {
    function Square(x, y, div) {
        if (div === void 0) { div = null; }
        this.x = x;
        this.y = y;
        this.div = div;
        this.is_snake = false;
        this.food = false;
    }
    Square.prototype.coords = function () {
        return { x: this.x, y: this.y };
    };
    Square.prototype.bone = function () {
        this.div.classList.add("bone");
        this.is_snake = true;
    };
    Square.prototype.unbone = function () {
        this.div.classList.remove("bone");
        this.is_snake = false;
    };
    Square.prototype.makeFood = function () {
        this.div.classList.add("food");
        this.food = true;
    };
    Square.prototype.unfood = function () {
        this.div.classList.remove("food");
        this.food = false;
    };
    return Square;
}());
//# sourceMappingURL=square.js.map