/// <reference path='board.ts'/>
/// <reference path='bone.ts'/>
/// <reference path='snake.ts'/>
var BoardGenerator = (function () {
    function BoardGenerator() {
    }
    BoardGenerator.generate = function (width, height) {
        var prevPause = Board.paused;
        BoardGenerator.prepareBoard(width, height);
        var boardC = document.getElementById("board-container");
        boardC.innerHTML = "";
        var boardDiv = document.createElement("div");
        boardDiv.setAttribute("id", "board");
        boardC.appendChild(boardDiv);
        for (var y = 1; y <= height; y += 1) {
            var row = document.createElement("div");
            row.setAttribute("class", "row");
            for (var x = 1; x <= width; x += 1) {
                row.appendChild(BoardGenerator.generateSquare(x, y));
            }
            boardDiv.appendChild(row);
        }
        Board.prepareMatrix();
        var board_div = document.getElementById("board");
        board_div.style.height = height * 12 + "px";
        board_div.style.width = width * 12 + "px";
        Board.paused = prevPause;
        BoardGenerator.addSnake();
    };
    BoardGenerator.generateSquare = function (x, y) {
        var square = document.createElement("div");
        square.setAttribute("class", "square" + " " + "square-" + x + "-" + y);
        square.setAttribute("data-x", x + "");
        square.setAttribute("data-y", y + "");
        return square;
    };
    BoardGenerator.prepareBoard = function (width, height) {
        Board.paused = true;
        Board.WIDTH = width;
        Board.LENGTH = height;
        Board.NUMBER_OF_SQUARES = width * height;
    };
    BoardGenerator.addSnake = function () {
        Board.snake = new Snake;
        var r_s = Board.randomSquare();
        Board.snake.addBone(new Bone(r_s));
        Board.snake.addBone(new Bone(Board.find(r_s.x + 1, r_s.y)));
        Board.addFood();
    };
    return BoardGenerator;
}());
//# sourceMappingURL=board_generator.js.map