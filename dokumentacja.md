# Dokumentacja Snake
## Kompilacja
`$ cd ts && tsc --sourcemap ts/*.ts -t ES5`
### Uruchomienie programu
Otwarcie pliku index.html w przeglądarce
### Uruchomienie testów
Otwarcie pliku test/SpecRunner.html w przeglądarce
## Instrukcja obsługi

a / strzałka w lewo / h : skręt w lewo

s / strzałka w dół / j : skręt w dół

d / strzałka w prawo / l : skręt w prawo

w / strzałka w górę / k : skręt w górę

g : pokazanie formularza generowania planszy

f7 : zapisanie gry

f8 : wczytanie gry

p : pauzowanie gry
## Pliki statyczne
css/main.css – Plik ze stylami

index.html – Widok html

## app.ts
> Skrypt uruchamiający grę

`ready(fn)` Uruchamia fn po załadowaniu się DOM

`onReady` Tworzy planszę do gry i rozpoczyna pętlę główną

## Board
> Klasa planszy

`static OPPOSITE_DIRECTION` Mapa ruch -> ruch przeciwny

`static WIDTH` Szerokość planszy

`static LENGTH` Długość planszy

`static NUMBER_OF_SQUARES` Liczba komórek w planszy

`static matrix` Plansza "właściwa"

`static paused` Zmienna mówiąca, czy gra jest zapauzowana

`static snake` Wąż planszy

`static prepareMatrix() : void` Funkcja tworząca dwuwymiarową planszę

`static find(x: number, y: number) : Square`
Funkcja szukająca komórki o danych współrzędnych

`static findByCoords(coords: Object) : Square`
Funkcja analogiczna do find, ale współrzędne pobiera z obiektu,
będącego argumentem

`static transform(x: number, dim: string) : number`
Funkcja dostosowująca współrzędną do rozmiarów planszy

`static randomSquare() : Square` Funkcja zwracająca losową komórkę

`static addFood() : void` Funkcja dodająca jedzenie do planszy

`static togglePause() : boolean` Funkcja (od)pauzująca grę

## BoardGenerator
> Generator planszy

`static generate(width: number, height: number) : void`
Tworzy plansze w html o danej szerokości i długości ustalając przy tym
odpowiednie parametry w klasie Board

`static generateSquare(x: number, y: number) : HTMLElement`
Tworzy element HTML o dopowiednich parametrach

`static prepareBoard(width: number, height: number) : void`
Ustala zmienna w klasie Board

`static addSnake() : void` Tworzy nowego węża

## Square
> Klasa komórki
#### Zmienne instancyjne
`x: number` Współrzędna x

`y: number` Współrzędna y

`div: Element` Element html

`is_snake: boolean` Zmienna mówiąca, czy komórka jest częścią węża

`food: boolean` Zmienna mówiąca, czy komórka jest jedzeniem
#### Metody
`constructor(x, y, div = null)` Konstruktor

`coords() : Object` Zwraca obiekt ze współrzędnymi

`bone() : void` Dodaje klase "bone" do `div` i ustalająca
`is_snake` na `true`

`unbone() : void` Funkcja przeciwna do `bone()`

`makeFood() : void` Dodaje klasę "food" do `div` i ustawiająca
atrybut `food` na `true`

`unfood() : void` Funkcja przeciwna do `makeFood()`


## Bone
> Dziedziczy po Square

> Klasa komórki węża


#### Zmienne intancyjne
> Zmienne jak w `Cell`

#### Metody
`constructor(square: Square)` Konstruktor; oprócz zwykłego
przypisania atrybutów deleguje `unbone()` do `square`

`newCoords(direction = this.snake.direction) : Object` Tworzy
współrzędne następnej komórki.

`head() : void` Dodaje klasę "head" do `div`

`unhead() : void` Funkcja przeciwna do `head()`

## KeyboardHandler
> Klasa odpowiadająca za wykonanie czynności zależnie od naciśniętego
> klawisza

`static MOVEMENTS` Mapa klawicz -> ruch węża

`static PAUSE` Mapa klawiszy pauzujących

`static SAVER` Mapa zapisywania/wczytywania gry

`static GENERATOR` Klawisze pokazujące formularz generatora planszy

## Saver
> Klasa odpowiadająca za zapisywanie i wczytywanie zapisanej gry

`static save() : void` Zapisuje grę w Local Storage

`static load() : boolean` Wczytuje grę z Local Storage

`static refreshFood() : void` Ustawia atrybut food odpowiedniej komórce
planszy

## Snake
> Klasa węża

### Metody klasowe
`static loadFromMap(map: Object) : Snake` Tworzy węża z postaci
zserializowanej
#### Zmienne instancyjna
`bones: Array<Bone>` Lista komórek węża

`private _direction: string` Aktualny kierunek węża

`private _last_direction: string` Poprzedni kierunek węża

`lost: boolean` Zmienna mówiąca, czy wąż przegrał

#### Metody
`constructor(direction = 'right')` Konstruktor

`addBone(bone: Bone) : void` Dodaję komórkę do listy

`head() : Bone` Zwraca pierwszą komórkę

`move() : void` Rusza wężem

`set direction(dir: string)` Setter direction

`get direction() : string` Getter direction

`validDirection(dir: string) : boolean` Sprawdza czy podany kierunek nie
jest przeciwnym do aktualnego

`popLast() : void` Usuwa ostatnią komórkę

`lose() : void` Ustawia `lost` na `true`

`isHead(Bone) : boolean` Porównuje argument z głową

`won() : boolean` Sprawdza, czy długść węża nie jest równa wielkości
planszy

`serialize() : Object` Zwraca zserializowaną postać węża

## Ewentualne zmiany i ich trudność
- Różne rodzaje jedzenia

  Trudność w implementacji zależałaby od tego, jak rodzaj wpłyałby na
  rozgrywkę. Automatyczne znikanie byłoby dość proste do implementacji,
  gdyż wystarczyłoby tylko lekko zmodyfikować główną pętlę gry.
  Przedłużanie węża o więcej niż 1 komórkę wymagałoby już przepisania
  znacznej części klasy Snake
- Więcej niż 1 wąż

  Zmiany byłyby niewielkie; Board zamiast atrybutu pojedynczego węża
  posiadałby ich listę; należałoby także zmienić przypisanie klawiszy
